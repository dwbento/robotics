﻿// JavaScript Document
function require(jspath){
	document.write('<script type="text/javascript" src="'+jspath+'"><\/script>');
}

//jQ
require("../js/jquery.min.js");
require("../js/jquery-ui-1.10.4.min.js"); /*required for some anim props*/

//interface
require("../js/Init.js");
require("../js/Init_NonEval.js");

//parsers
require("../js/math_parser.js");
require("../js/xml_parser.js");
//require("../js/xml_parser-mod.js");
require("lessonStrtr.js"); 

//analytics
//require("../js/analytics.js");    //I commented this line out

//video!
//require("../js/ev3_video_initializer.js");

//CS2N Feedback button. Do not remove FILE for DVD. It breaks all pages.
require("../js/CS2NFeedback.js");


