﻿// JavaScript Document


$(document).ready(function() {
	var doc = $(document);
	var slideSpeed = 300
		
	openAllBtn = doc.find(".c-index_toggleAll").children("a");
	
	//1-a. bind click for "Open All" button
	openAllBtn.click(function(){
		
		//if button says "Expand"
		if ($(".c-index_toggleAll").attr("isClicked") != "True"){
			
			//sets toggleAll to "True"
			$(".c-index_toggleAll").attr("isClicked","True");
						
			doc.find(".c-index_clampBar").each(function(){
			
				//toggle clampBars, if they are closed
				if ($(this).attr("isClicked") != "True"){
					$(this).next().slideToggle(slideSpeed);
				}
				//sets clampBar to "True"
				$(this).attr("isClicked","True");
			});
			
		
		//if button says "Collapse"
		}else {
			$(".c-index_toggleAll").attr("isClicked","False");
			doc.find(".c-index_clampBar").each(function(){
				
				//toggle clampBars, if they are open
				if ($(this).attr("isClicked") != "False"){
					$(this).next().slideToggle(slideSpeed);
				}
				//change property
				$(this).attr("isClicked","False");
			});	
		}	
	});
	
	//1-b. page initially expands all
	$(".c-index_toggleAll").attr("isClicked","True");
	
	//-----------------------------------------------
				
	//2. bind click for each of the drop-arrow buttons
	doc.find(".c-index_clampBar").each(function(){
		
					
		//bind each bars
		$(this).click(function(){
			$(this).next().slideToggle(slideSpeed);
			
			if ($(this).attr("isClicked") != "True"){
				$(this).attr("isClicked","True");
			} else {
				$(this).attr("isClicked","False");
			}						
		}); //end: click binding
					
	}); //end: each()
	
	
	// ---- NA -----
	//3. Open first clamp to help user get started 
	// var firstClamp = doc.find(".c-index_clampBar").eq(0);
	// firstClamp.next().slideToggle("slow");

	
}); //end. ready()